import java.util.Scanner;
public class SwapTwoNumbers{
public static void main(String []args){
int num1,num2;
int temp;
Scanner scanner=new Scanner(System.in);
System.out.println("enter num1");
num1=scanner.nextInt();
System.out.println("enter num2");
num2=scanner.nextInt();
System.out.println("before swapping");
System.out.println("num1=" +num1);
System.out.println("num2=" +num2);
temp=num1;
num1=num2;
num2=temp;
System.out.println("after swapping");
System.out.println("num1=" +num1);
System.out.println("num2=" +num2);
}
}