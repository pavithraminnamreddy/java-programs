//sum of two numbers using abstraction
package com.pavitra;

import java.util.Scanner;

abstract class First {

    int a, b, s;

    abstract void input();
    abstract void add();
    abstract void result();
    Scanner sc = new Scanner(System.in);
}

class Main extends First {

    void input() {
        System.out.print("Enter Two Numbers :");
        a = sc.nextInt();
        b = sc.nextInt();
    }

    void add() {
        s = a + b;
    }

    void result() {
        System.out.print("\nThe Sum of Two Numebrs is :" + s);
    }

    public static void main(String args[]) {
        Main st = new Main();
        st.input();
        st.add();
        st.result();
    }
}

